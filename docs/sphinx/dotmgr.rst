Source code
===========

dotmgr.cli module
-----------------

.. automodule:: dotmgr.cli
   :members:
   :undoc-members:
   :show-inheritance:

dotmgr.manager module
---------------------

.. automodule:: dotmgr.manager
   :members:
   :undoc-members:
   :show-inheritance:

dotmgr.paths module
-------------------

.. automodule:: dotmgr.paths
   :members:
   :undoc-members:
   :show-inheritance:

dotmgr.repository module
------------------------

.. automodule:: dotmgr.repository
   :members:
   :undoc-members:
   :show-inheritance:

dotmgr.transformer module
-------------------------

.. automodule:: dotmgr.transformer
   :members:
   :undoc-members:
   :show-inheritance:
