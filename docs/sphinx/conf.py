"""Sphinx configuration is contained in pyproject.toml, which is simply read here.
"""
from sphinx_pyproject import SphinxConfig
SphinxConfig("../../pyproject.toml", globalns=globals())
