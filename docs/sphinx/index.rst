.. dotmgr documentation master file, created by
   sphinx-quickstart on Fri Oct 30 15:08:09 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Welcome to dotmgr's documentation!
==================================


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README.md
   dotmgr


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
